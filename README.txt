
Description of what the module does.




Dependencies
------------
 * Content


Install
-------
(explain the steps.

1) Copy the filefield_zip folder to the modules folder in your installation.

2) Enable the module using Administer -> Site building -> Modules
   (/admin/build/modules).

3) Go to the filefield settings page using Administer -> Site Configuration
   -> FileField Zip (admin/settings/filefieldzip).
   Choose the filefields to use for creation of zip files. Alternative choose a
   folder to where the zip files should be stored on the server.

4) Upload files to the selected filefield.
