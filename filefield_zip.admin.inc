<?php
/**
 * @file
 *
 * Filefield Zip admin settings.
 *
 */

function filefield_zip_admin_settings_form(&$form_state) {

  $form['ffz_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => FALSE,
  );

  // Get default path of the sites files folder from web root.
  $zip_directory = _filefield_zip_directory();

  $form['ffz_settings']['directory'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to where you want to place your zip files'),
    '#field_prefix' => $zip_directory['full_directory_path'] . 'filefieldzip/',
    '#default_value' => variable_get('directory', ''),
    '#size' => 40,
    '#required' => FALSE,
    '#description' => t('Specify the path to where you want to place the zip folder.'),
    '#attributes' => array('class' => 'ffz-directory'),
  );


  $form['ffz_settings']['filefield_zip_nodetype'] = _get_content_types_with_fields();

  return system_settings_form($form);

}